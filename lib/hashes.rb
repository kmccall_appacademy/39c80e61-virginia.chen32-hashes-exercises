# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  lengths=Hash.new(0)
  str.split(" ").each_with_index do |word|
    lengths[word.downcase]=word.length
  end
  lengths
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  sorted_hash = hash.sort_by { |k,v| v}
  sorted_hash[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k,v|
    older[k]=v
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  uniq_letters = word.split("").uniq
  counter = Hash.new(0)
  uniq_letters.each do |letter|
    counter[letter]=word.downcase.count(letter)
  end
  counter
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  counter = Hash.new(0)
  arr.each do |el|
    counter[el]=arr.count(el)
  end
  counter.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  even_odds = {:even => 0, :odd => 0}
  numbers.each do |num|
    if num.even?
      even_odds[:even]+=1
    else even_odds[:odd]+=1
    end
  end
  even_odds
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowel_counts=Hash.new(0)
  vowels_only = string.downcase.delete("bcdfghjklmnpqrstvwxyz!,.?:;")
  vowels = "aeiou"
  vowels_only.split("").each do |char|
    if vowels.include?(char)
      vowel_counts[char]+=1
    end
  end
  vowel_counts.sort_by { |k,v| v}[-1][0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  late_bdays = students.select{ |name,bday| bday >= 7}
  student_combos = []
  late_students = late_bdays.keys
  counter1 = 0
  counter2 = 0
  while counter1<late_students.length-1
    counter2=counter1+1
    while counter2<late_students.length
      student_combos << [late_students[counter1],late_students[counter2]]
      counter2+=1
    end
    counter1+=1
  end
  student_combos
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  count=Hash.new(0)
  specimens.each do |specimen|
    count[specimen] += 1
  end
  return (count.length**2)*(count.values.min/count.values.max)
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  vandalcount = character_count(vandalized_sign)
    normalcount = character_count(normal_sign)
    vandalcount.each do |char,count|
      if normalcount[char] < count
        return FALSE
      end
    end
    return TRUE
end

def character_count(str)
  counter = Hash.new(0)
  str.downcase.delete("!?.,:; ").chars.each do |char|
    counter[char] += 1
  end
  counter
end
